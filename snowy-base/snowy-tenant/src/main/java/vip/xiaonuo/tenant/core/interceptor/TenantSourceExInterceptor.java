package vip.xiaonuo.tenant.core.interceptor;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import vip.xiaonuo.core.context.constant.ConstantContextHolder;
import vip.xiaonuo.core.context.login.LoginContextHolder;
import vip.xiaonuo.core.dbs.CurrentDataSourceContext;
import vip.xiaonuo.core.pojo.login.SysLoginUser;
import vip.xiaonuo.core.tenant.consts.TenantConstants;
import vip.xiaonuo.core.tenant.context.TenantCodeHolder;
import vip.xiaonuo.core.tenant.context.TenantDbNameHolder;
import vip.xiaonuo.core.util.HttpServletUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author dongxiayu
 * @description 租户的多数据源切换的拦截器
 * @date 2022/7/1 22:49
 */
@Slf4j
@Component
public class TenantSourceExInterceptor implements HandlerInterceptor {

    /**
     * 目标方法执行前
     * 该方法在控制器处理请求方法前执行，其返回值表示是否中断后续操作
     * 返回 true 表示继续向下执行，返回 false 表示中断后续操作
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean handleFlag = false;
        try {
            String token = HttpServletUtil.getTokenFromReferer();
            if(ObjectUtil.isEmpty(token)) {
                // 从header获取不到则从参数中获取
                token = HttpServletUtil.getTokenFromParam();
            }
            // 根据系统总开关来进行aop
            if (StrUtil.isNotBlank(token)) {
                // 只要传入token并且正确，都需要继续执行
                SysLoginUser sysLoginUser = LoginContextHolder.me().getSysLoginUserByToken(token);
                if(ObjectUtil.isNotEmpty(sysLoginUser)) {
                    handleFlag = true;
                }
                // 当前用户已经登陆并且租户信息不为空，则决定是否切换数据源
                Dict tenantInfo = sysLoginUser.getTenants();
                if (tenantInfo != null) {
                    // 获取当前用户登录的租户标识，切换数据源
                    String tenantDbName = tenantInfo.getStr(TenantConstants.TENANT_DB_NAME);
                    if (StrUtil.isNotBlank(tenantDbName)) {
                        CurrentDataSourceContext.setDataSourceType(tenantDbName);
                        log.debug(">>> 多租户AOP--TenantSourceExInterceptor--设置数据源为：" + tenantDbName);
                    }
                }
            }
        } catch (Exception e){
            clearTenantContextData();
        }
        return handleFlag;
    }


    /**
     * 目标方法执行后
     * 该方法在控制器处理请求方法调用之后、解析视图之前执行
     * 可以通过此方法对请求域中的模型和视图做进一步修改
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        clearTenantContextData();
    }

    /**
     * 页面渲染后
     * 该方法在视图渲染结束后执行
     * 可以通过此方法实现资源清理、记录日志信息等工作
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        clearTenantContextData();
    }

    /**
     * @description 清空数据源信息
     * @author dongxiayu
     * @date 2022/7/2 1:12
     * @return
     **/
    private void clearTenantContextData(){
        log.debug(">>> 多租户AOP--TenantSourceExInterceptor--清空数据源信息！");
        CurrentDataSourceContext.clearDataSourceType();
        TenantCodeHolder.remove();
        TenantDbNameHolder.remove();
    }

}