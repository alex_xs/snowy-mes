/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.customcode.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.customcode.param.CustomCodeParam;
import vip.xiaonuo.modular.customcode.service.CustomCodeService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 自定义编号规则控制器
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
@RestController
public class CustomCodeController {

    @Resource
    private CustomCodeService customCodeService;

    /**
     * 查询自定义编号规则
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @GetMapping("/customCode/page")
    @BusinessLog(title = "自定义编号规则_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(CustomCodeParam customCodeParam) {
        return new SuccessResponseData(customCodeService.page(customCodeParam));
    }

    /**
     * 添加自定义编号规则
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @PostMapping("/customCode/add")
    @BusinessLog(title = "自定义编号规则_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(CustomCodeParam.add.class) CustomCodeParam customCodeParam) {
            customCodeService.add(customCodeParam);
        return new SuccessResponseData();
    }

    /**
     * 删除自定义编号规则，可批量删除
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @PostMapping("/customCode/delete")
    @BusinessLog(title = "自定义编号规则_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(CustomCodeParam.delete.class) List<CustomCodeParam> customCodeParamList) {
            customCodeService.delete(customCodeParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑自定义编号规则
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @PostMapping("/customCode/edit")
    @BusinessLog(title = "自定义编号规则_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(CustomCodeParam.edit.class) CustomCodeParam customCodeParam) {
            customCodeService.edit(customCodeParam);
        return new SuccessResponseData();
    }

    /**
     * 查看自定义编号规则
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @GetMapping("/customCode/detail")
    @BusinessLog(title = "自定义编号规则_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(CustomCodeParam.detail.class) CustomCodeParam customCodeParam) {
        return new SuccessResponseData(customCodeService.detail(customCodeParam));
    }

    /**
     * 自定义编号规则列表
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @GetMapping("/customCode/list")
    @BusinessLog(title = "自定义编号规则_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(CustomCodeParam customCodeParam) {
        return new SuccessResponseData(customCodeService.list(customCodeParam));
    }

    /**
     * 导出系统用户
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @GetMapping("/customCode/export")
    @BusinessLog(title = "自定义编号规则_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(CustomCodeParam customCodeParam) {
        customCodeService.export(customCodeParam);
    }

    /**
     * 自定义编号规则表列表
     *
     * @author sxy
     * @date 2022-06-28 09:32:04
     */
    @Permission
    @GetMapping("/customCode/InformationList")
    @BusinessLog(title = "自定义编号规则数据库表列表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData InformationList() {
        return ResponseData.success(customCodeService.getInformationTable());
    }
}
