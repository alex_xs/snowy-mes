/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.workreport.entity.WorkReport;
import vip.xiaonuo.modular.workreport.param.WorkReportParam;
import vip.xiaonuo.modular.workreport.service.WorkReportService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 报工表控制器
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
@RestController
public class WorkReportController {

    @Resource
    private WorkReportService workReportService;

    /**
     * 查询报工表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @GetMapping("/workReport/page")
    @BusinessLog(title = "报工表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkReportParam workReportParam) {
        return new SuccessResponseData(workReportService.page(workReportParam));
    }

    /**
     * 添加报工表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @PostMapping("/workReport/add")
    @BusinessLog(title = "报工表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkReportParam.add.class) WorkReportParam workReportParam) {
            workReportService.add(workReportParam);
        return new SuccessResponseData();
    }

    /**
     * 删除报工表，可批量删除
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @PostMapping("/workReport/delete")
    @BusinessLog(title = "报工表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkReportParam.delete.class) List<WorkReportParam> workReportParamList) {
            workReportService.delete(workReportParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑报工表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @PostMapping("/workReport/edit")
    @BusinessLog(title = "报工表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkReportParam.edit.class) WorkReportParam workReportParam) {
            workReportService.edit(workReportParam);
        return new SuccessResponseData();
    }

    /**
     * 查看报工表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @GetMapping("/workReport/detail")
    @BusinessLog(title = "报工表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkReportParam.detail.class) WorkReportParam workReportParam) {
        return new SuccessResponseData(workReportService.detail(workReportParam));
    }


    /**
     * 审批报工表
     *
     * @author 楊楊
     * @date 2022-06-30 14:08:42
     */
    @Permission
    @PostMapping("/workReport/approval")
    @BusinessLog(title = "报工表_审批", opType = LogAnnotionOpTypeEnum.CHANGE_STATUS)
    public ResponseData approval( WorkReportParam workReportParam) {
       workReportService.approval(workReportParam);
        return new SuccessResponseData();
    }


    /**
     * 报工表列表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @GetMapping("/workReport/list")
    @BusinessLog(title = "报工表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkReportParam workReportParam) {
        return new SuccessResponseData(workReportService.list(workReportParam));
    }

    /**
     * 导出系统用户
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    @Permission
    @GetMapping("/workReport/export")
    @BusinessLog(title = "报工表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkReportParam workReportParam) {
        workReportService.export(workReportParam);
    }

}
