package vip.xiaonuo.modular.invOut.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.invIn.entity.InvIn;
import vip.xiaonuo.modular.invOut.entity.InvOut;

/**
 * @author 苏辰
 */
@Data
public class invOutResult extends InvOut {
    /**
     * 仓库名称
     */
    @Excel(name = "仓库名称")
    private String warHouName;
}
