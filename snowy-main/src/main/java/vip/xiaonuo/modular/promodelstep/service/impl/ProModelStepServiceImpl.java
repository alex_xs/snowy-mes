/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.promodelstep.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.promodelstep.entity.ProModelStep;
import vip.xiaonuo.modular.promodelstep.enums.ProModelStepExceptionEnum;
import vip.xiaonuo.modular.promodelstep.mapper.ProModelStepMapper;
import vip.xiaonuo.modular.promodelstep.param.ProModelStepParam;
import vip.xiaonuo.modular.promodelstep.service.ProModelStepService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * 产品型号步骤service接口实现类
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
@Service
public class ProModelStepServiceImpl extends ServiceImpl<ProModelStepMapper, ProModelStep> implements ProModelStepService {

    @Override
    public PageResult<ProModelStep> page(ProModelStepParam proModelStepParam) {
        QueryWrapper<ProModelStep> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(proModelStepParam)) {

            // 根据步骤名称 查询
            if (ObjectUtil.isNotEmpty(proModelStepParam.getName())) {
                queryWrapper.lambda().like(ProModelStep::getName, proModelStepParam.getName());
            }
            // 根据产品型号id 查询
            if (ObjectUtil.isNotEmpty(proModelStepParam.getProModelId())) {
                queryWrapper.lambda().eq(ProModelStep::getProModelId, proModelStepParam.getProModelId());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<ProModelStep> list(ProModelStepParam proModelStepParam) {
        return this.list();
    }

    @Override
    public void add(ProModelStepParam proModelStepParam) {
        ProModelStep proModelStep = new ProModelStep();
        BeanUtil.copyProperties(proModelStepParam, proModelStep);
        this.save(proModelStep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<ProModelStepParam> proModelStepParamList) {
        proModelStepParamList.forEach(proModelStepParam -> {
            this.removeById(proModelStepParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(ProModelStepParam proModelStepParam) {
        ProModelStep proModelStep = this.queryProModelStep(proModelStepParam);
        BeanUtil.copyProperties(proModelStepParam, proModelStep);
        this.updateById(proModelStep);
    }

    @Override
    public ProModelStep detail(ProModelStepParam proModelStepParam) {
        return this.queryProModelStep(proModelStepParam);
    }

    /**
     * 获取产品型号步骤
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    private ProModelStep queryProModelStep(ProModelStepParam proModelStepParam) {
        ProModelStep proModelStep = this.getById(proModelStepParam.getId());
        if (ObjectUtil.isNull(proModelStep)) {
            throw new ServiceException(ProModelStepExceptionEnum.NOT_EXIST);
        }
        return proModelStep;
    }

    @Override
    public void export(ProModelStepParam proModelStepParam) {
        List<ProModelStep> list = this.list(proModelStepParam);
        PoiUtil.exportExcelWithStream("SnowyProModelStep.xls", ProModelStep.class, list);
    }

}
