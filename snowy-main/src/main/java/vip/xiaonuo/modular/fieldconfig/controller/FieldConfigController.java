/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.fieldconfig.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.fieldconfig.param.FieldConfigParam;
import vip.xiaonuo.modular.fieldconfig.service.FieldConfigService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 字段配置控制器
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
@RestController
public class FieldConfigController {

    @Resource
    private FieldConfigService fieldConfigService;

    /**
     * 查询字段配置
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @GetMapping("/fieldConfig/page")
    @BusinessLog(title = "字段配置_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(FieldConfigParam fieldConfigParam) {
        return new SuccessResponseData(fieldConfigService.page(fieldConfigParam));
    }

    /**
     * 添加字段配置
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @PostMapping("/fieldConfig/add")
    @BusinessLog(title = "字段配置_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(FieldConfigParam.add.class) FieldConfigParam fieldConfigParam) {
            fieldConfigService.add(fieldConfigParam);
        return new SuccessResponseData();
    }

    /**
     * 删除字段配置，可批量删除
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @PostMapping("/fieldConfig/delete")
    @BusinessLog(title = "字段配置_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(FieldConfigParam.delete.class) List<FieldConfigParam> fieldConfigParamList) {
            fieldConfigService.delete(fieldConfigParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑字段配置
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @PostMapping("/fieldConfig/edit")
    @BusinessLog(title = "字段配置_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(FieldConfigParam.edit.class) FieldConfigParam fieldConfigParam) {
            fieldConfigService.edit(fieldConfigParam);
        return new SuccessResponseData();
    }

    /**
     * 查看字段配置
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @GetMapping("/fieldConfig/detail")
    @BusinessLog(title = "字段配置_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(FieldConfigParam.detail.class) FieldConfigParam fieldConfigParam) {
        return new SuccessResponseData(fieldConfigService.detail(fieldConfigParam));
    }

    /**
     * 字段配置列表
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @GetMapping("/fieldConfig/list")
    @BusinessLog(title = "字段配置_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(FieldConfigParam fieldConfigParam) {
        return new SuccessResponseData(fieldConfigService.list(fieldConfigParam));
    }

    /**
     * 导出系统用户
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    @Permission
    @GetMapping("/fieldConfig/export")
    @BusinessLog(title = "字段配置_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(FieldConfigParam fieldConfigParam) {
        fieldConfigService.export(fieldConfigParam);
    }

}
