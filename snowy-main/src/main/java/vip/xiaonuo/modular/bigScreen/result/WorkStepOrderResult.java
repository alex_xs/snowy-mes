package vip.xiaonuo.modular.bigScreen.result;

import lombok.Data;

@Data
public class WorkStepOrderResult {
    //工序名称
    private String name;
    //生产任务数
    private Integer taskNum;
    //进度
    private Double plannedSpeed;
    //不良品率
    private Double defectRate;
    //良品数
    private Integer goodNum;
    //不良品数
    private Integer badNum;

}
