/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.task.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.*;
import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("dw_task")
public class Task extends BaseEntity {


    /**
     * 不良品数
     */
    @Excel(name = "不良品数")
    private Integer badNum;
    /**
     * 排序号
     */
    private Integer sortNum;

    /**
     * 实际结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "实际结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
   // @Excel(name = "实际结束时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date factEndTime;

    /**
     * 实际开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone="GMT+8")
    @Excel(name = "实际开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd ", width = 20)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
   // @Excel(name = "实际开始时间")
    private Date factStaTime;

    /**
     * 良品数
     */
    @Excel(name = "良品数")
    private Integer goodNum;
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 计划结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "计划结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
  //  @Excel(name = "计划结束时间")
    private Date plaEndTime;

    /**
     * 计划数
     */
    @Excel(name = "计划数")
    private Integer plaNum;

    /**
     * 计划开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "计划开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
   // @Excel(name = "计划开始时间")
    private Date plaStartTime;
    /**
     * 产品id
     */
    @Excel(name = "产品id")
    private Long proId;

    /**
     * 产品类型id
     */
    @Excel(name = "产品类型id")
    private Long proTypeId;


    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;

    /**
     * 工单id 工单编号
     */
    @Excel(name = "工单id")
    private Long workOrderId;

    /**
     * 工序id
     */
    @Excel(name = "工序id")
    private Long workStepId;
    /**
     * 编码
     */
    @Excel(name = "编码")
    private String code;
    /**
     * 报工权限
     */
    @Excel(name = "报工权限")
    private String reportRight;
    /**
     * 不良品项
     */
//    @Excel(name = "不良品项")
//    private String mulBadItem;

}
