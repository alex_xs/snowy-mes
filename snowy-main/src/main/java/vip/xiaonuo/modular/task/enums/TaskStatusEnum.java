package vip.xiaonuo.modular.task.enums;

import lombok.Getter;

/**
 * @author l
 */
@Getter
public enum TaskStatusEnum {
    /**
     * 未开始
     */
    NOT_START(-1,"未开始"),
    /**
     * 执行中
     */
    EXECUTION(0,"执行中"),
    /**
     * 已结束
     */
    FINISH(1,"已结束");



    private final Integer code;

    private final String message;

    TaskStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
