import { axios } from '@/utils/request'

/**
 * 查询工艺路线
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRoutePage (parameter) {
  return axios({
    url: '/workRoute/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工艺路线列表
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRouteList (parameter) {
  return axios({
    url: '/workRoute/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工艺路线
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRouteAdd (parameter) {
  return axios({
    url: '/workRoute/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工艺路线
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRouteEdit (parameter) {
  return axios({
    url: '/workRoute/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工艺路线
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRouteDelete (parameter) {
  return axios({
    url: '/workRoute/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出工艺路线
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
export function workRouteExport (parameter) {
  return axios({
    url: '/workRoute/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
