import { axios } from '@/utils/request'

/**
 * 查询出入库明细
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailPage (parameter) {
  return axios({
    url: '/invDetail/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 出入库明细列表
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailList (parameter) {
  return axios({
    url: '/invDetail/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加出入库明细
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailAdd (parameter) {
  return axios({
    url: '/invDetail/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑出入库明细
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailEdit (parameter) {
  return axios({
    url: '/invDetail/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除出入库明细
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailDelete (parameter) {
  return axios({
    url: '/invDetail/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出出入库明细
 *
 * @author wz
 * @date 2022-06-09 09:26:28
 */
export function invDetailExport (parameter) {
  return axios({
    url: '/invDetail/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
